﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour {

    public static int SIZE = 6;
    public static float pngSIZE = 1.4f;
    public static float animSpeed = 5f;
}
