﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionScript : MonoBehaviour {

    void Update()
    {
        if (GamePlay.Instance.CURRENT == GamePlay.State.playerAction && Input.GetMouseButtonDown(0))
        {
            CheckoutMouseClick(Input.mousePosition);
        }
    }

    private void CheckoutMouseClick(Vector2 mousePos)
    {
        RaycastHit2D[] hits;
        Vector2 ray = Camera.main.ScreenToWorldPoint(mousePos);
        hits = Physics2D.RaycastAll(ray, Vector2.zero);
        foreach (RaycastHit2D hit in hits)
        {
            string hit_name = hit.transform.name;
            int i = GetIndex(0, hit_name);
            int j = GetIndex(1, hit_name);
            GamePlay.Instance.PlayerClicksOn(i, j); 
        }
    }

    private int GetIndex(int numberOfIndex, string text)
    {
        string help_string = "";
        int toReturn = 0;
        switch(numberOfIndex)
        {
            case 0:
                for(int i=0;i<text.Length;i++)
                {
                    if (text[i].ToString()=="_")
                    {
                        break;
                    }
                    help_string += text[i];
                }
                break;
            case 1:
                bool startWriting = false;
                for(int i=0;i<text.Length;i++)
                {
                    if (text[i].ToString()=="_")
                    {
                        startWriting = true;
                        i++;
                    }
                    if (startWriting)
                    {
                        help_string += text[i];
                    }
                }
                break;
        }
        toReturn = int.Parse(help_string);
        return toReturn;
    }
}
