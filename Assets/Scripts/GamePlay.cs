﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlay : MonoBehaviour {

    public static GamePlay Instance;

    private int lastTarget_i = -1;
    private int lastTarget_j = -1;

    private int score = 0;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public enum State
    {
        initialization,
        playerAction,
        animationOrChecking,
        endGame
    }
    private State current;
    public State CURRENT
    {
        get
        {
            return current;
        }
        set
        {
            current = value;
            switch (value)
            {
                case State.initialization: break;
            }
        }
    }

    public void PlayerClicksOn(int i, int j)
    {
        if (lastTarget_i == -1)
        {
            //first target
            MapScript.Instance.SetTarget(i, j);
            lastTarget_i = i;
            lastTarget_j = j;
        }
        else
        {
            int range_i = Mathf.Abs(lastTarget_i - i);
            int range_j = Mathf.Abs(lastTarget_j - j);
            if (range_i+range_j==1)
            {
                //swap
                StartCoroutine(SwapAction(i,j));
            }
            else
            {
                MapScript.Instance.SetTarget(i, j);
                lastTarget_i = i;
                lastTarget_j = j;
            }
        }
    }
    public void MapReady()
    {
        //check for matches and any action
        StartCoroutine(CheckForStartGame());
    }
    private IEnumerator CheckForStartGame()
    {
        CURRENT = State.animationOrChecking;
        int result_score = 0;
        while (true)
        {
            while (true)
            {
                result_score = MapScript.Instance.CheckDelete();
                if (result_score > 0)
                {
                    score += result_score;
                    UIManager.Instance.ChangeScore(score);
                    yield return StartCoroutine(MapScript.Instance.DeleteAndGenerateNew());
                    yield return null;
                }
                else
                {
                    break;
                }
            }
            if (MapScript.Instance.CheckForAnyAction())
            {
                break;
            }
            else
            {
                yield return StartCoroutine(MapScript.Instance.Mix());
            }
            yield return null;
        }

        CURRENT = State.playerAction;
        yield break;
    }
    private IEnumerator SwapAction(int i, int j)
    {
        CURRENT = State.animationOrChecking;

        //swap animation
        MapScript.Instance.SetNoTarget();
        yield return StartCoroutine(MapScript.Instance.SwapCells(lastTarget_i,lastTarget_j,i,j));
 
        //check result
        int result_score = MapScript.Instance.CheckDelete();
        if (result_score==0)
        {
            yield return StartCoroutine(MapScript.Instance.SwapCells(lastTarget_i, lastTarget_j, i, j));
            lastTarget_i = -1;

            CURRENT = State.playerAction;
            yield break;
        }
        else
        {
            score += result_score;
            UIManager.Instance.ChangeScore(score);
            yield return StartCoroutine(MapScript.Instance.DeleteAndGenerateNew());
            lastTarget_i = -1;
        }

        while (true)
        {
            while (true)
            {
                result_score = MapScript.Instance.CheckDelete();
                if (result_score > 0)
                {
                    score += result_score;
                    UIManager.Instance.ChangeScore(score);
                    yield return StartCoroutine(MapScript.Instance.DeleteAndGenerateNew());
                    yield return null;
                }
                else
                {
                    break;
                }
            }
            if (MapScript.Instance.CheckForAnyAction())
            {
                break;
            }
            else
            {
                yield return StartCoroutine(MapScript.Instance.Mix());
            }
            yield return null;
        }
        
        CURRENT = State.playerAction;

        yield break;
    }

    public void MixBordPressed()
    {
        if (CURRENT == State.playerAction)
        {
            StartCoroutine(MixForTest());
        }
    }
    private IEnumerator MixForTest()
    {
        CURRENT = State.animationOrChecking;
        MapScript.Instance.SetNoTarget();
        lastTarget_i = -1;
        yield return StartCoroutine(MapScript.Instance.Mix());
        int result_score = 0;
        while (true)
        {
            while (true)
            {
                result_score = MapScript.Instance.CheckDelete();
                if (result_score > 0)
                {
                    score += result_score;
                    UIManager.Instance.ChangeScore(score);
                    yield return StartCoroutine(MapScript.Instance.DeleteAndGenerateNew());
                    yield return null;
                }
                else
                {
                    break;
                }
            }
            if (MapScript.Instance.CheckForAnyAction())
            {
                break;
            }
            else
            {
                yield return StartCoroutine(MapScript.Instance.Mix());
            }
            yield return null;
        }
        CURRENT = State.playerAction;
        yield break;
    }
}
