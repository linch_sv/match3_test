﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScript : MonoBehaviour {

    public static MapScript Instance;

    private int[,] delete_array;
    private int[,] map_array;
    private SpriteRenderer[,] mapSprite_array;
    private Transform my_Transform;

    // 0 - red
    // 1 - blue
    // 2 - green
    // 3 - yellow
    public Sprite[] sprite_array;
    public GameObject cell_prefab;
    public Transform target_Transform;
    private SpriteRenderer target_Sprite;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        my_Transform = transform;
        target_Sprite = target_Transform.GetComponent<SpriteRenderer>();
        GenerateMap();     
    }

    private void GenerateMap()
    {
        map_array = new int[Global.SIZE*2, Global.SIZE];
        for (int i = 0; i < Global.SIZE*2; i++)
        {
            for (int j = 0; j < Global.SIZE; j++)
            {
                map_array[i, j] = -1;
            }
        }
        /*
        map_array = new int[,] {{0,1,1,1,1,1 },
                                {0,3,2,3,2,3 },
                                {0,1,0,1,0,1},
                                {0,3,2,3,2,3  },
                                {0,1,0,1,0,1 },
                                {0,3,2,3,2,3  },
                                {-1,-1,-1,-1,-1,-1},
                                {-1,-1,-1,-1,-1,-1},
                                {-1,-1,-1,-1,-1,-1},
                                {-1,-1,-1,-1,-1,-1},
                                {-1,-1,-1,-1,-1,-1},
                                {-1,-1,-1,-1,-1,-1}
                                };*/

        mapSprite_array = new SpriteRenderer[Global.SIZE*2, Global.SIZE];
        
        for(int i=0;i<Global.SIZE;i++)
        {
            for(int j=0;j< Global.SIZE; j++)
            {
                int nextNumber = GetRandomNumber();
                if (CheckNext(nextNumber,i,j))
                {
                    map_array[i, j] = nextNumber;
                }
                else
                {
                    j--;
                }
            }
        }
        StartCoroutine(ShowMap());
    }
    private int GetRandomNumber()
    {
        return Mathf.RoundToInt(Random.Range(0, 4));
    }
    private bool CheckNext(int nextNumber, int i, int j)
    {
        if (i>=2)
        {
            if (map_array[i-1,j] == nextNumber && map_array[i - 2, j] == nextNumber)
            {
                return false;
            }
        }
        if (j>=2)
        {
            if (map_array[i, j-1] == nextNumber && map_array[i, j-1] == nextNumber)
            {
                return false;
            }
        }
        return true;
    }
    private IEnumerator ShowMap()
    {
        GameObject help_GO;
        for (int i = 0; i < Global.SIZE; i++)
        {
            for (int j = 0; j < Global.SIZE; j++)
            {
                help_GO = Instantiate(cell_prefab, new Vector3(j * Global.pngSIZE, -i * Global.pngSIZE, 0), Quaternion.identity, my_Transform);
                help_GO.name = i.ToString() + "_" + j.ToString();
                mapSprite_array[i, j] = help_GO.GetComponent<SpriteRenderer>();
                mapSprite_array[i, j].sprite = sprite_array[map_array[i, j]];
                yield return null;
            }
        }
        
        GamePlay.Instance.MapReady();
        yield break;
    }

    public void SetNoTarget()
    {
        target_Sprite.enabled = false;
    }
    public void SetTarget(int i, int j)
    {
        target_Transform.position = new Vector3(j * Global.pngSIZE, -i * Global.pngSIZE, 1f);
        target_Sprite.enabled = true;
    }

    public IEnumerator SwapCells(int i1, int j1, int i2,int j2)
    {
        float progress = 0f;

        //animation
        Transform cell1 = mapSprite_array[i1, j1].transform;
        Transform cell2 = mapSprite_array[i2, j2].transform;

        Vector3 start = cell1.position;
        Vector3 end = cell2.position;

        while(progress<1f)
        {
            progress += Time.deltaTime * Global.animSpeed;
            cell1.position = Vector3.Lerp(start, end, progress);
            cell2.position = Vector3.Lerp(end, start, progress);
            yield return null;
        }
        //change cells info
        int help_int = map_array[i1, j1];
        map_array[i1, j1] = map_array[i2, j2];
        map_array[i2, j2] = help_int;
        cell1.name = i2.ToString() + "_" + j2.ToString();
        cell2.name = i1.ToString() + "_" + j1.ToString();
        SpriteRenderer help_spr = mapSprite_array[i1, j1];
        mapSprite_array[i1, j1] = mapSprite_array[i2, j2];
        mapSprite_array[i2, j2] = help_spr;

        yield return null;
    }
    public IEnumerator DeleteAndGenerateNew()
    {
        //1) delete cells=1 from delete_array
        List<int> indexForAnim_i = new List<int>();
        List<int> indexForAnim_j = new List<int>();
        for(int i=0;i<delete_array.GetLength(0);i++)
        {
            for(int j=0;j<delete_array.GetLength(1);j++)
            {
                if (delete_array[i,j] == 1)
                {
                    indexForAnim_i.Add(i);
                    indexForAnim_j.Add(j);
                    map_array[i, j] = -1;
                }
            }
        }
        float progress = 0f;
        //int count = indexForAnim_i.Count...
        while(progress<1f)
        {
            progress += Time.deltaTime*Global.animSpeed;
            for(int i=0;i<indexForAnim_i.Count;i++)
            {
                mapSprite_array[indexForAnim_i[i], indexForAnim_j[i]].color = new Color(1f, 1f, 1f, 1f - progress);
            }
            yield return null;
        }
        //2)Add and animate new cells

        int maxIndex = 0;
        int plusIndex = 0;
        for(int j=0;j< Global.SIZE; j++)
        {
            plusIndex = 0;
            for(int i=0;i<Global.SIZE;i++)
            {
                if (map_array[i,j] == -1)
                {
                    map_array[Global.SIZE + plusIndex, j] = GetRandomNumber();
                    //mapSprite_array[i, j].sprite = sprite_array[map_array[Global.SIZE + plusIndex, j]];
                    mapSprite_array[i, j].color = new Color(1f, 1f, 1f, 1f);
                    mapSprite_array[Global.SIZE + plusIndex, j] = mapSprite_array[i, j];
                    mapSprite_array[Global.SIZE + plusIndex, j].sprite = sprite_array[map_array[Global.SIZE + plusIndex, j]];
                    mapSprite_array[Global.SIZE + plusIndex, j].name = (Global.SIZE + plusIndex).ToString() + "_" + j.ToString();
                    mapSprite_array[i, j] = null;
                    plusIndex++;
                }
            }
            if (maxIndex<plusIndex)
            {
                maxIndex = plusIndex;
            }
        }
        
        Vector3 start = new Vector3(0,Global.pngSIZE,0);
        Vector3 end = new Vector3(0,0,0);
        Vector3 help;
        int[] startIndex = new int[6];
        //
        for (int steps = 0; steps < maxIndex; steps++)
        {
            progress = 0f;
            for (int j = 0; j < Global.SIZE; j++)
            {
                startIndex[j] = -1;
                for (int i = 0; i < Global.SIZE; i++)
                {
                    if (map_array[i, j] == -1)
                    {
                        startIndex[j] = i;
                        break;
                    }
                }
            }
            while (progress < 1f)
            {
                progress += Time.deltaTime * Global.animSpeed;
                help = Vector3.Lerp(start, end, progress);
                for (int j = 0; j < 6; j++)
                {
                    if (startIndex[j] >= 0)
                    {
                        for (int i = startIndex[j] + 1; i < Global.SIZE * 2; i++)
                        {
                            if (mapSprite_array[i, j] != null)
                            {
                                mapSprite_array[i, j].transform.position = new Vector3(j * Global.pngSIZE, (-i + 1) * Global.pngSIZE, 0) - help;
                            }
                        }
                    }
                }
                yield return null;
            }
            //change cells info
            for (int j = 0; j < Global.SIZE; j++)
            {
                if (startIndex[j] >= 0)
                {
                    for (int i = startIndex[j]; i < Global.SIZE * 2 - 1; i++)
                    {
                        map_array[i, j] = map_array[i + 1, j];
                        mapSprite_array[i, j] = mapSprite_array[i + 1, j];
                        if (mapSprite_array[i, j] != null)
                        {
                            mapSprite_array[i, j].transform.name = i.ToString() + "_" + j.ToString();
                        }
                    }
                    map_array[Global.SIZE * 2 - 1, j] = -1;
                    mapSprite_array[Global.SIZE * 2 - 1, j] = null;
                }
            }
        }
        yield return null;
    }
    public bool CheckForAnyAction()
    {
        for(int i=0;i<Global.SIZE;i++)
        {
            for(int j=0;j<Global.SIZE;j++)
            {
                if (i<Global.SIZE-1)
                {
                    if (CheckForMatch(1,i,j))
                    {
                        return true;
                    }
                }
                if (j<Global.SIZE-1)
                {
                    if (CheckForMatch(0, i, j))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    private bool CheckForMatch(int side,int _i,int _j)
    {
        // 1 - down, 0 - right

        int save_int = 0;
        if (side == 1)
        {
            save_int = map_array[_i, _j];
            map_array[_i, _j] = map_array[_i + 1, _j];
            map_array[_i + 1, _j] = save_int;
        }
        else
        {
            save_int = map_array[_i, _j];
            map_array[_i, _j] = map_array[_i, _j+1];
            map_array[_i, _j+1] = save_int;
        }

        int sum = 1;
        for(int i=1;i < Global.SIZE;i++)
        {
            if (map_array[i,_j] == map_array[i-1,_j])
            {
                sum++;
                if (sum == 3)
                {
                    break;
                }
            }
            else
            {
                sum = 1;
            }
        }
        if (sum<3)
        {
            sum = 1;
            for (int j = 1; j < Global.SIZE; j++)
            {
                if (map_array[_i, j] == map_array[_i, j-1])
                {
                    sum++;
                    if (sum == 3)
                    {
                        break;
                    }
                }
                else
                {
                    sum = 1;
                }
            }
        }
        if (sum<3)
        {
            sum = 1;
            if (side == 1)
            {
                for (int j = 1; j < Global.SIZE; j++)
                {
                    if (map_array[_i+1, j] == map_array[_i+1, j - 1])
                    {
                        sum++;
                        if (sum == 3)
                        {
                            break;
                        }
                    }
                    else
                    {
                        sum = 1;
                    }
                }
            }
            else
            {
                for (int i = 1; i < Global.SIZE; i++)
                {
                    if (map_array[i, _j+1] == map_array[i - 1, _j+1])
                    {
                        sum++;
                        if (sum == 3)
                        {
                            break;
                        }
                    }
                    else
                    {
                        sum = 1;
                    }
                }
            }
        }

        if (side == 1)
        {
            save_int = map_array[_i, _j];
            map_array[_i, _j] = map_array[_i + 1, _j];
            map_array[_i + 1, _j] = save_int;
        }
        else
        {
            save_int = map_array[_i, _j];
            map_array[_i, _j] = map_array[_i, _j + 1];
            map_array[_i, _j + 1] = save_int;
        }
        if (sum==3)
        {
            return true;
        }
        return false;
    }

    public int CheckDelete()
    {
        int score = 0;
        int counter = 1;
        delete_array = new int[Global.SIZE, Global.SIZE];
        
        //check to right
        for(int i=0;i<delete_array.GetLength(0);i++)
        {
            counter = 1;
            for (int j=1;j<delete_array.GetLength(1);j++)
            {
                if (map_array[i,j] == map_array[i,j-1])
                {
                    counter++;
                }
                else
                {
                    if (counter>=3)
                    {
                        score += 10 + (counter - 3) * 5;
                        SetToDelete(false, counter, i, j);
                    }
                    counter = 1;
                }
            }
            if (counter >= 3)
            {
                score += 10 + (counter - 3) * 5;
                SetToDelete(false, counter, i, Global.SIZE);
            }
        }
        //check to bot
        for (int j = 0; j < delete_array.GetLength(0); j++)
        {
            counter = 1;
            for (int i = 1; i < delete_array.GetLength(1); i++)
            {
                if (map_array[i, j] == map_array[i-1, j])
                {
                    counter++;
                }
                else
                {
                    if (counter >= 3)
                    {
                        score += 10 + (counter - 3) * 5;
                        SetToDelete(true, counter, i, j);
                    }
                    counter = 1;
                }
            }
            if (counter >= 3)
            {
                score += 10 + (counter - 3) * 5;
                SetToDelete(true, counter, Global.SIZE, j);
            }
        }
        return score;
    }   
    private void SetToDelete(bool iOrj, int count, int i, int j)
    {
        if (!iOrj)
        {
            for (int k = 0; k < count; k++)
            {
                delete_array[i, j - k - 1] = 1;
            }
        }
        else
        {
            for (int k = 0; k < count; k++)
            {
                delete_array[i - k-1, j] = 1;
            }
        }
    }
    public IEnumerator Mix()
    {
        //random mix count
        UIManager.Instance.ShowErrorMessage(true);
        int count = Mathf.RoundToInt(Random.Range(14, 20));
        if (count % 2 > 0)
        {
            count++;
        }

        //delete same cells
        List<int> index_i = new List<int>();
        List<int> index_j = new List<int>();
        int i_toAdd = 0;
        int j_toAdd = 0;
        bool dontAdd;
        for (int c = 0; c < count; c++)
        {
            dontAdd = false;
            i_toAdd = Mathf.RoundToInt(Random.Range(0, 6));
            j_toAdd = Mathf.RoundToInt(Random.Range(0, 6));
            for (int list_index = 0; list_index < index_i.Count; list_index++)
            {
                if (index_i[list_index] == i_toAdd && index_j[list_index] == j_toAdd)
                {
                    c--;
                    dontAdd = true;
                }
            }
            if (!dontAdd)
            {
                index_i.Add(i_toAdd);
                index_j.Add(j_toAdd);
            }
        }
        if (index_i.Count % 2 > 0)
        {
            index_i.RemoveAt(index_i.Count - 1);
            index_j.RemoveAt(index_j.Count - 1);
        }

        //mix animation
        List<Vector3> pos_list = new List<Vector3>();
        for (int i = 0; i < index_i.Count; i++)
        {
            pos_list.Add(mapSprite_array[index_i[i], index_j[i]].transform.position);
        }

        float progress = 0f;
        Vector3 start;
        Vector3 end;
        while (progress < 1f)
        {
            progress += Time.deltaTime;
            for (int i = 0; i < index_i.Count / 2; i++)
            {
                start = pos_list[i * 2];
                end = pos_list[i * 2 + 1];
                mapSprite_array[index_i[i * 2], index_j[i * 2]].transform.position = Vector3.Lerp(start, end, progress);
                mapSprite_array[index_i[i * 2 + 1], index_j[i * 2 + 1]].transform.position = Vector3.Lerp(end, start, progress);
            }
            yield return null;
        }
        //change cells info
        int help_int = 0;
        SpriteRenderer help_Sprite;
        for (int i = 0; i < index_i.Count / 2; i++)
        {
            help_int = map_array[index_i[i * 2], index_j[i * 2]];
            map_array[index_i[i * 2], index_j[i * 2]] = map_array[index_i[i * 2 + 1], index_j[i * 2 + 1]];
            map_array[index_i[i * 2 + 1], index_j[i * 2 + 1]] = help_int;
            help_Sprite = mapSprite_array[index_i[i * 2], index_j[i * 2]];
            mapSprite_array[index_i[i * 2], index_j[i * 2]] = mapSprite_array[index_i[i * 2 + 1], index_j[i * 2 + 1]];
            mapSprite_array[index_i[i * 2 + 1], index_j[i * 2 + 1]] = help_Sprite;
            mapSprite_array[index_i[i * 2], index_j[i * 2]].transform.name = index_i[i * 2].ToString() + "_" + index_j[i * 2].ToString();
            mapSprite_array[index_i[i * 2 + 1], index_j[i * 2 + 1]].transform.name = index_i[i * 2 + 1].ToString() + "_" + index_j[i * 2 + 1].ToString();
        }

        UIManager.Instance.ShowErrorMessage(false);
        
        yield break;
    }
    /*
    private class Cell
    {
        public Transform my_Transform;
        public SpriteRenderer my_Sprite;
        public int i;
        public int j;
        public int number;
    }*/
}
