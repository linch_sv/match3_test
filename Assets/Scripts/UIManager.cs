﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    public Text score_Text;
    public Text error;
        
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeScore(int _score)
    {
        score_Text.text = _score.ToString();
    }

    public void ShowErrorMessage(bool state)
    {
        error.enabled = state;
    }

}
